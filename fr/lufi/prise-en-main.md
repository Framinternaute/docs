# Prise en main

![framadrop explication numérotée](images/drop_mail_redaction_numero.png)

  1. bouton pour télécharger le fichier
  2. bouton pour copier le lien de téléchargement du fichier
  3. bouton pour [envoyer un mail avec le(s) lien(s)](#envoyer-les-liens-par-mail)
  4. bouton pour supprimer le fichier

## Envoyer les liens par mail

Une fois votre fichier déposé dans Framadrop, vous pouvez envoyer le lien par mail aux destinataires de votre choix. Pour ce faire, cliquez sur **Envoyer tous les liens par mail** ou sur l'icône <i class="fa fa-fw fa-envelope"></i> (voir `3` dans l'image ci-dessus)

Vous arrivez alors sur l'interface de personnalisation où vous pourrez :
  * indiquer les adresses destinataires dans le champ **Adresses mails séparées par des virgules** ; vous pouvez ne mettre qu'une seule adresse (sans virgule à la fin) ou plusieurs (séparées par des virgules : `adresse@mail.fr,adresse2@mail.fr,adresse3@mail.fr`)
  * indiquer un sujet dans **Sujet du mail**
  * personnaliser le corps du mail en enlevant ce dont vous n'avez pas besoin

Pour envoyer :

  * **Envoyer avec le serveur** : envoi directement depuis votre navigateur
  * **Envoyer avec votre propre logiciel de mail** : utilisera Thunderbird ou Outlook (ou autres)

![envoi de mail via le navigateur](images/drop_mail_redaction.png)
