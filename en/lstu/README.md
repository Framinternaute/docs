# Framalink / Huitre

[Frama.link](https://frama.link) or [Huit.re](https://huit.re) is a free (as in free speech) simple online URL shortener.

  1. Paste a long URL to make it shorter.
  - If needed, you can choose the text for the shortened link
  - Next step: share your short link with whoever you wish.

Frama.link is derived from [Lstu](https://lstu.fr/) created and maintained by [Luc Didry](https://fiat-tux.fr/).

## See more

  * [Try Frama.link](https://frama.link) or [Huit.re](https://huit.re)
  * [Features](fonctionnalites.md)
  * [Framalink's API](https://frama.link/api)
  * [Contribute](https://framagit.org/framasoft/framalink/)
  * [Support us](https://soutenir.framasoft.org/en)
