Mastodon User's Guide
=====================

## Intro

Mastodon is a social network application based on the OStatus protocol. It behaves a lot like other social networks, especially Twitter, with one key difference - it is open-source and anyone can start their own server (also called an "*instance*"), and users of any instance can interact freely with those of other instances (called "*federation*"). Thus, it is possible for small communities to set up their own servers to use amongst themselves while also allowing interaction with other communities.

#### Decentralization and Federation

Mastodon is a system decentralized through a concept called "*federation*" - rather than depending on a single person or organization to run its infrastructure, anyone can download and run the software and run their own server. Federation means different Mastodon servers can interact with each other seamlessly, similar to e.g. e-mail.

As such, anyone can download Mastodon and e.g. run it for a small community of people, but any user registered on that instance can follow, send, and read posts from other Mastodon instances (as well as servers running other OStatus-compatible services, such as GNU Social and postActiv). This means that not only is users' data not inherently owned by a company with an interest in selling it to advertisers, but also that if any given server shuts down its users can set up a new one or migrate to another instance, rather than the entire service being lost.

Within each Mastodon instance, usernames just appear as `@username`, similar to other services such as Twitter. Users from other instances appear, and can be searched for and followed, as `@user@servername.ext` - so e.g. `@gargron` on the `mastodon.social` instance can be followed from other instances as `@gargron@mastodon.social`).

Posts from users on external instances are "*federated*" into the local one, i.e. if `user1@mastodon1` follows `user2@gnusocial2`, any posts `user2@gnusocial2` makes appear in both `user1@mastodon1`'s Home feed and the public timeline on the `mastodon1` server. Mastodon server administrators have some control over this and can exclude users' posts from appearing on the public timeline; post privacy settings from users on Mastodon instances also affect this, see below in the [Toot Privacy](User-guide.html#toot-privacy) section.


* [Getting Started](User-guide.html#getting-started)
  * [Setting Up Your Profile](User-guide.html#setting-up-your-profile)
  * [E-Mail Notifications](User-guide.html#e-mail-notifications)
  * [Text Posts](User-guide.html#text-posts)
    * [Content Warnings](User-guide.html#content-warnings)
    * [Hashtags](User-guide.html#hashtags)
    * [Boosts and Favourites](User-guide.html#boosts-and-favourites)
  * [Posting Images](User-guide.html#posting-images)
  * [Following Other Users](User-guide.html#following-other-users)
  * [Notifications](User-guide.html#notifications)
  * [Mobile Apps](User-guide.html#mobile-apps)
  * [The Federated Timeline](User-guide.html#the-federated-timeline)
  * [The Local Timeline](User-guide.html#the-local-timeline)
  * [Searching](User-guide.html#searching)
* [Privacy, Safety and Security](User-guide.html#privacy-safety-and-security)
  * [Two-Factor Authentication](User-guide.html#two-factor-authentication)
  * [Account Privacy](User-guide.html#account-privacy)
  * [Toot Privacy](User-guide.html#toot-privacy)
  * [Blocking](User-guide.html#blocking)
  * [Reporting Toots or Users](User-guide.html#reporting-toots-or-users)
